import React from 'react';
import { StyleSheet, Text, View, AppRegistry } from 'react-native';
import WelcomeScreen from './src/App'

export default class App extends React.Component {
  render() {
    return (
      <WelcomeScreen />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
