import { createStackNavigator, TabNavigator } from 'react-navigation'
import Welcome from './components/Welcome'


const WelcomeScreen = createStackNavigator(
  {
    WelcomeScreen:{ screen: Welcome}
  },
  {
    initialRouteName: 'WelcomeScreen',
    headerMode: 'none'
  }
);

export default WelcomeScreen